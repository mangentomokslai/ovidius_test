<?php
class Ovidius_Test_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        echo 'test/index action';
    }

    public function dumplayoutAction()
    {
        $params = $this->getRequest()->getParams(); // parse parameters (key/value pairs as array) from url
        $outputAsXml = array_key_exists('xml', $params) ? true : false; // check if xml param is present


        /*
            Couple of notes:
                * <pre></pre> - tag used to encapsulate preformatted text.
                * htmlentities($string) - converts certain characters to html entities.
                  It's required in order for browser to render text verbatim instead of parsing it as HTML.

            Layout call sequence:
              $this->loadLayout() - initialization
              $this->getLayout() - calls Mage::getSingleton('core/layout') which gets layout object from registry
              $this->getUpdate() - gets the layout update object (Mage_Core_Layout_Update)
              $this->asString() - converts layout update array to string
        */
        $layoutDump = $this->loadLayout()->getLayout()->getUpdate()->asString();

        if($outputAsXml) {
            $this->getResponse()
                ->setHeader('Content-Disposition', 'attachment; filename=layout.xml')
                ->setHeader('Content-type', 'text/plain')
                ->setBody($layoutDump);
        } else {
            echo "<pre>" . htmlentities($layoutDump) . "</pre>";
        }
    }

    public function getCategoriesAction()
    {
        $rootCategories = Mage::getResourceModel('catalog/category_collection')
                        ->addFieldToFilter('level', 1)
                        ->addAttributeToSelect('name');

        foreach ($rootCategories as $rc) {
            echo 'ROOT CATEGORY:<br>';
            Zend_Debug::dump($rc->debug());
            $rcChildren = $rc->getChildren();
            $rcChildrenIds = explode(',', $rcChildren);

            foreach ($rcChildrenIds as $rcChildId) {
                $child = Mage::getModel('catalog/category')->load($rcChildId);
                Zend_Debug::dump($child->debug());
            }

        }

    }

}
